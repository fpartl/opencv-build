@echo off

:: Pokud uživatel nezadá argument, skript skončí
if [%1]==[] goto usage

:: Umístění sestavovaných souborů
set "DEPS_DIR=%cd%"
set "BUILD_DIR=%DEPS_DIR%/opencv/build_%1"
set "INSTALL_DIR=%DEPS_DIR%/opencv_build_%1"

:: Vždy začínáme od začátku
if exist "%BUILD_DIR%" rd /s /q "%BUILD_DIR%"
if exist "%INSTALL_DIR%" rd /s /q "%INSTALL_DIR%"

mkdir "%BUILD_DIR%"
cd "%BUILD_DIR%"

@echo on
:: Samotné sestavení knihovny
cmake -DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_opencv_apps=OFF -DCMAKE_INSTALL_PREFIX="%INSTALL_DIR%" -DCMAKE_BUILD_TYPE="%1" -DBUILD_SHARED_LIBS=ON -DOPENCV_EXTRA_MODULES_PATH="%DEPS_DIR%/opencv_contrib/modules" -DBUILD_LIST=core,imgproc,imgcodecs,dnn ..

pause

cmake --build . --config %1
cmake --install . --config %1

@echo off

:: Závěrečný úklid
cd %DEPS_DIR%
rd /s /q "%BUILD_DIR%"
goto quit

:usage
echo "build_opencv.bat <Release|Debug|RelWithDebInfo>"

:quit