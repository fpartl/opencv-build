#!/bin/bash

DEPS_DIR=$(pwd)

# Install adresář odstraním, jestli již existuje.
if [ -d "$DEPS_DIR/opencv_build_$1" ]; then 
    rm -rf "$DEPS_DIR/opencv_build_$1"
fi
# Build adresář odstraním, jestli již existuje.
if [ -d "$DEPS_DIR/opencv/build_$1" ]; then 
    rm -rf "$DEPS_DIR/opencv/build_$1"
fi

# Instalace závislostí knihovny FFmpeg (volitelně)
#sudo apt-get install ffmpeg
#sudo apt-get install libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libavresample-dev

mkdir "$DEPS_DIR/opencv/build_$1"
cd "$DEPS_DIR/opencv/build_$1"

cmake -DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_opencv_apps=OFF  -DCMAKE_INSTALL_PREFIX:PATH="$DEPS_DIR/opencv_build_$1" -DCMAKE_BUILD_TYPE:STRING="$1" -DBUILD_SHARED_LIBS=ON -DOPENCV_EXTRA_MODULES_PATH="$DEPS_DIR/opencv_contrib/modules" -DBUILD_LIST=core,imgproc,imgcodecs,dnn ..

read -p "Press enter to continue..."

make -j8
make install

cd $DEPS_DIR
rm -rf "$DEPS_DIR/opencv/build_$1"